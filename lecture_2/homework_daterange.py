from datetime import datetime, timedelta, date
import argparse

def date_toolkits(date_input_str):
    
    format_str = "%Y%m%d"
    date_datetime = datetime.strptime(date_input_str, format_str)
    
    week_number = date_datetime.isocalendar()[1]
    week_day = date_datetime.isoweekday()
    date_range = dict()
    
    if week_day == 5:
        date_range['week_number'] = week_number
        date_range['start_date'] = (date_datetime - timedelta(7)).strftime(format_str)
        date_range['end_date'] = (date_datetime - timedelta(1)).strftime(format_str)
        date_range['days_included'] = 7

    elif week_day == 6:
        if week_number < 52:
            date_range['week_numberber'] = week_number + 1
        else :
            date_range['week_numberber'] = 1
        date_range['start_date'] = (date_datetime - timedelta(1)).strftime(format_str)
        date_range['end_date'] = (date_datetime - timedelta(1)).strftime(format_str)
        date_range['days_included'] = 1

    elif week_day == 7:
        if week_number < 52:
            date_range['week_numberber'] = week_number + 1
        else :
            date_range['week_numberber'] = 1
        date_range['start_date'] = (date_datetime - timedelta(2)).strftime(format_str)
        date_range['end_date'] = (date_datetime - timedelta(1)).strftime(format_str)
        date_range['days_included'] = 2

    elif week_day == 1:
        date_range['week_number'] = week_number
        date_range['start_date'] = (date_datetime - timedelta(3)).strftime(format_str)
        date_range['end_date'] = (date_datetime - timedelta(1)).strftime(format_str)
        date_range['days_included'] = 3

    elif week_day == 2:
        date_range['week_number'] = week_number
        date_range['start_date'] = (date_datetime - timedelta(4)).strftime(format_str)
        date_range['end_date'] = (date_datetime - timedelta(1)).strftime(format_str)
        date_range['days_included'] = 4

    elif week_day == 3:
        date_range['week_number'] = week_number
        date_range['start_date'] = (date_datetime - timedelta(5)).strftime(format_str)
        date_range['end_date'] = (date_datetime - timedelta(1)).strftime(format_str)
        date_range['days_included'] = 5

    elif week_day == 4:
        date_range['week_number'] = week_number
        date_range['start_date'] = (date_datetime - timedelta(6)).strftime(format_str)
        date_range['end_date'] = (date_datetime - timedelta(1)).strftime(format_str)
        date_range['days_included'] = 6
        
    return date_range


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='this function is for calculate date range ')
    # Add arguments
    parser.add_argument("-d", "--date", required=True, type=str,
                        help="date to check, Ymd")
    
    args = parser.parse_args()
    date_input_str = args.date
    
    date_range = date_toolkits(date_input_str)
    print(date_range)
    
    
# python function_3_daterange.py -d 20200717