def fibs(n):
    """
    input is a int
    """
    result = [0, 1]
    for i in range(n-2):
        result.append(result[-2] + result[-1])
        
    return result

def print_func(result):
    print("this is a reuslt: {}".format(result))
    a = 11
    return a
    
def main():
    input_num = 11
    lst = fibs(input_num)
    a = print_func(lst)
    print(a)
    
    
if __name__=="__main__":
    main()