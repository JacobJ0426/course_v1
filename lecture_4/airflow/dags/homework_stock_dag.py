from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.contrib.sensors.file_sensor import FileSensor
from datetime import timedelta, date, datetime

from pandas_datareader import data as wb
from datetime import datetime, timedelta, date
import time
import os

import sys, os

sys.path.append("/home/alston_yan_project_a/python_operator/")
import stock_price
sys.path.remove("/home/alston_yan_project_a/python_operator/")

# This is airflow arg area:
default_args = {
    "owner": "alston",
    "depends_on_past": False,
    "start_date": datetime(2020, 8, 14),
    "retries": 1,
    'retry_delay': timedelta(minutes=5)
}

dag = DAG(dag_id='homework_stock_dag', 
          description='Simple tutorial DAG',
          default_args=default_args,
          schedule_interval='0 12 * * *', 
          catchup=False)


    

def print_hello():
    return 'Hello world!'


    
dummy_operator = DummyOperator(task_id='dummy_task', 
                               retries=3, 
                               dag=dag)

hello_operator = PythonOperator(task_id='hello_task', 
                                python_callable=print_hello, 
                                dag=dag)

get_stock = PythonOperator(task_id='get_stock', 
                            python_callable=stock_price.main, 
                            dag=dag)

# [dummy_operator, hello_operator] >> get_stock
hello_operator >> get_stock


# test: airflow test homework_stock_dag get_stock 2020-08-14
# --> pip install pandas_datareader

