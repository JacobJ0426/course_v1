import os
import smtplib

import yaml
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText



def img_str(x):
    grill = """<tr><td> <img src="cid:{}" width="80%"> </td></tr>""".format(x)
    return grill


def email_html_render(png_location, file_name):
    current_workdir = os.path.abspath(os.path.join(os.getcwd(), "."))
    conf = yaml.load(open(current_workdir + '/credentials/email_notification.yaml'))

    ### prepare string to replace in html file ###
    imagedir = png_location
    imagepath = dict()

    fname = file_name + '.png'
    if fname not in os.listdir(imagedir):
        raise IOError('Trend image failed to generate')
    temp = imagedir + fname
    imagepath[file_name] = temp
    img_str_replace = img_str(file_name)
    ### render html & email ###
    # Define these once; use them twice!
    strFrom = "victoria.alston.2018@gmail.com"
    strTo = ["yanwy.weiran@outlook.com"]

    # Create the root message and fill in the from, to, and subject headers
    msgRoot = MIMEMultipart('related')
    msgRoot['Subject'] = 'Indeed Monitoring System'
    msgRoot['From'] = strFrom
    msgRoot['To'] = ','.join(strTo)
    msgRoot.preamble = 'This is a multi-part message in MIME format.'

    # Encapsulate the plain and HTML versions of the message body in an
    # 'alternative' part, so message agents can decide which they want to display.
    msgAlternative = MIMEMultipart('alternative')
    msgRoot.attach(msgAlternative)

    print ('redering email')
    text = open('./html/templates.html').read()
    text = text.replace('<!--image_here-->', img_str_replace)
    msgText = MIMEText(text, 'html')
    msgAlternative.attach(msgText)

    # This example assumes the image is in the current directory
    for i in imagepath.keys():
        fp = open(imagepath[i], 'rb')
        msgImage = MIMEImage(fp.read())
        fp.close()
        # Define the image's ID as referenced above
        msgImage.add_header('Content-ID', '<{}>'.format(i))
        msgRoot.attach(msgImage)
    # Send the email (this example assumes SMTP authentication is required)

    s = smtplib.SMTP_SSL('smtp.gmail.com', 465)
    s.ehlo()
    s.login(user=conf['user']['email'], password=conf['user']['password'])
    print ('sending email...')
    s.sendmail(strFrom, strTo, msgRoot.as_string())
    result = s.quit()
    if result[0] != 221:
        raise RuntimeError('email sending status error')
    else:
        print ('Success')


def send_email_with_plot():

    current_workdir = os.path.abspath(os.path.join(os.getcwd(), "."))
    folder = current_workdir + "/image/"
    file_name = 'output'

    print("Descision to send email")
    email_html_render(folder, file_name)


if __name__ == "__main__":
    send_email_with_plot()