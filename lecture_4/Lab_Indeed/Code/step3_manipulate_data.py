# [START imports]
import pandas as pd
import os
from datetime import timedelta, date, datetime


# [END imports]

class pd_read_gbq():
    def __init__(self, **kwargs):
        """

        self.query = kwargs.pop('query', '')


        try:
            self.PROJECT_ID = kwargs.pop('project_id')
        except KeyError:
            self.PROJECT_ID = os.environ['PROJECT_ID']

        """

    def external_pandas_readgbq(self, **kwargs):
        query = kwargs.pop('query')

        try:
            PROJECT_ID = kwargs.pop('project_id')
        except KeyError:
            PROJECT_ID = os.environ['PROJECT_ID']

        # Reading bigquery
        df = pd.read_gbq(query,
                         project_id=PROJECT_ID,
                         dialect='standard',
                         verbose=False)
        return df

    def pandas_process(self, df):
        # Export pandas to trigger
        return df

    def write_to_file(self, **kwargs):

        df = kwargs.pop('df')
        final_filename = kwargs.pop('final_filename')

        print("Write data into temp file in {}".format(final_filename))
        df.to_csv(final_filename,
                  index=False,
                  encoding='utf-8')
        print("Done")


if __name__ == "__main__":
    # Call function examples
    query = (
        """ 
        SELECT 
        *,
        'Data Analyst' AS job 
        FROM 
        `cantire-alston.indeed_api.indeed_data_*` 
        WHERE 
        _TABLE_SUFFIX = CAST(FORMAT_DATE("%Y%m%d", DATE_SUB(DATE(current_timestamp(), "America/New_York"), INTERVAL 0 DAY)) AS STRING);
        """)

    project_id = 'cantire-alston'

    # For user account
    # PANDAS_GBQ_CREDENTIALS_FILE = os.environ['PANDAS_GBQ_CREDENTIALS_FILE']

    current_workdir = os.path.abspath(os.path.join(os.getcwd(), "."))

    file_name = current_workdir + "/file/output.csv"

    x = pd_read_gbq()
    df = x.external_pandas_readgbq(query=query, project_id=project_id)
    df = x.pandas_process(df)
    x.write_to_file(df=df, final_filename=file_name)